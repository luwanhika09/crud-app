import 'package:flutter/material.dart';

class HouseCard extends StatelessWidget {
  final int index;
  final Map houseRule;
  final Function(Map) navigatorEdit;
  final Function(int) deleteById;
  const HouseCard({
    super.key,
    required this.index,
    required this.houseRule,
    required this.navigatorEdit,
    required this.deleteById,
  });

  @override
  Widget build(BuildContext context) {
    final id = houseRule['id'];
    return Card(
      child: ListTile(
        leading: CircleAvatar(child: Text('${index + 1}')),
        title: Text(
          houseRule['name'],
        ),
        subtitle: Text('Ative: ${houseRule['active']}'),
        trailing: PopupMenuButton(
          onSelected: (value) {
            if (value == 'edit') {
              // Abrir a Pagina
              navigatorEdit(houseRule);
            } else if (value == 'delete') {
              // Apagar e remover o item
              deleteById(id!);
            }
          },
          itemBuilder: (context) {
            return [
              const PopupMenuItem(
                value: 'edit',
                child: Text('Editar'),
              ),
              const PopupMenuItem(
                value: 'delete',
                child: Text('Apagar'),
              ),
            ];
          },
        ),
      ),
    );
  }
}
