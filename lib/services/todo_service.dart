import 'dart:convert';

import 'package:http/http.dart' as http;

/// All api call will be here
class TodoService {
  static Future<bool> deleteById(int id) async {
    final url = 'https://sys-dev.searchandstay.com/api/admin/house_rules/$id';
    const token =
        'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';
    final uri = Uri.parse(url);

    Map<String, String> headers = {
      'Authorization': token,
      'Content-Type': 'application/json'
    };
    final reponse = await http.delete(
      uri,
      headers: headers,
    );

    return reponse.statusCode == 200;
  }

  static Future<List?> fetchHouseRules() async {
    const url =
        'https://sys-dev.searchandstay.com/api/admin/house_rules?page=1&limit=10';
    const token =
        'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';
    final uri = Uri.parse(url);

    Map<String, String> headers = {
      'Authorization': token,
      'Content-Type': 'application/json'
    };
    final response = await http.get(
      uri,
      headers: headers,
    );

    if (response.statusCode == 200) {
      final json = jsonDecode(response.body) as Map;
      final result = json['data']['entities'] as List;
      final pagination = json['data']['pagination'];
      return result;
    } else {
      return null;
    }
  }

  static Future<bool> updateTodo(int id, Map body) async {
    final url = 'https://sys-dev.searchandstay.com/api/admin/house_rules/$id';
    const token =
        'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';
        
    final uri = Uri.parse(url);

    Map<String, String> headers = {
      'Authorization': token,
      'Content-Type': 'application/json'
    };
    final response = await http.put(
      uri,
      body: jsonEncode(body),
      headers: headers,
    );

    return response.statusCode == 200;
  }

  static Future<bool> addTodo(Map body) async {
    const url = 'https://sys-dev.searchandstay.com/api/admin/house_rules';
    const token =
        'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';
    final uri = Uri.parse(url);

    Map<String, String> headers = {
      'Authorization': token,
      'Content-Type': 'application/json'
    };
    final response = await http.post(
      uri,
      body: jsonEncode(body),
      headers: headers,
    );

    return response.statusCode == 200;
  }

  Future<bool> addTodoList({required String name, required int active}) async {
    const url = 'https://sys-dev.searchandstay.com/api/admin/house_rules';
    const token =
        'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';
    final uri = Uri.parse(url);

    Map<String, String> headers = {
      'Authorization': token,
      'Content-Type': 'application/json'
    };
    Map<String, dynamic> data = {
      'house_rules': {
        'name': name,
        'active': active,
      }
    };

    final response = await http.post(
      uri,
      body: jsonEncode(data),
      headers: headers,
    );

    return response.statusCode == 200;
  }
}
