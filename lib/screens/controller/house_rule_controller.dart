import 'package:crud_app/models/house_rules_model.dart';
import 'package:crud_app/screens/repository/house_rule_repository.dart';

class HouseRuleController {
  final repository = HouseRuleRepository();

  Future<List<Entities>> getEntities() async {
    return await repository.getAllHouseRules();
  }

  Future<Entities> createHouseEntity(Entities entity) async {
    return await repository.createHouseEntity(entity);
  }

  Future<Entities> updateHouseRule(int id, Entities entity) async {
    return await repository.updateHouseRule(id, entity);
  }

  Future<void> deleteHouseRule(int id) async {
    return await repository.deleteHouseRule(id);
  }


  Future<Entities> createHouseRule(String name, int active, int order) async {
    final entity = Entities(
      name: name,
      active: active,
      order: order,
    );

    return await repository.createHouseEntity(entity);
  }
  
}
