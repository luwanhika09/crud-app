import 'package:crud_app/models/house_rules_model.dart';
import 'package:crud_app/screens/add_page.dart';
import 'package:crud_app/services/todo_service.dart';
import 'package:crud_app/utils/snackbar_helper.dart';
import 'package:crud_app/widget/house_card.dart';
import 'package:flutter/material.dart';

class TodoListPage extends StatefulWidget {
  const TodoListPage({super.key});

  @override
  State<TodoListPage> createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  
  bool isLoading = true;
  late List<dynamic> _houseRules = [];
  List<Entities> entitiesList = [];

  @override
  void initState() {
    super.initState();
    fetchHouseRules();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('House Rules'),
      ),
      body: Visibility(
        visible: isLoading,
        replacement: RefreshIndicator(
          onRefresh: fetchHouseRules,
          child: Visibility(
            visible: _houseRules.isNotEmpty,
            replacement: Center(
              child: Text(
                'Não ha item na lista',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            child: ListView.builder(
              itemCount: _houseRules.length,
              padding: const EdgeInsets.all(8),
              itemBuilder: (context, index) {
                final houseRule = _houseRules[index];
                final id = houseRule['id'];
                return HouseCard(
                  index: index,
                  houseRule: houseRule,
                  navigatorEdit: navigateToEditPage,
                  deleteById: deleteById,
                );
              },
            ),
          ),
        ),
        child: const Center(child: CircularProgressIndicator()),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: navigateToAddPage,
        label: const Text('Adicionar'),
      ),
    );
  }

  Future<void> navigateToEditPage(Map item) async {
    final route = MaterialPageRoute(
      builder: (context) => AddTodoPage(todo: item),
    );
    await Navigator.push(context, route);
    setState(() {
      isLoading = true;
    });
    fetchHouseRules();
  }

  Future<void> navigateToAddPage() async {
    final route = MaterialPageRoute(
      builder: (context) => const AddTodoPage(),
    );
    await Navigator.push(context, route);
    setState(() {
      isLoading = true;
    });
    fetchHouseRules();
  }

  Future<void> deleteById(int id) async {
    // Delete the item
    final isSuccess = await TodoService.deleteById(id);
    if (isSuccess) {
      // Remove item from the list
      final filtered =
          _houseRules.where((element) => element['id'] != id).toList();
      setState(() {
        _houseRules = filtered;
      });
    } else {
      // Show Error
      showErrorMessage(context, message: 'Falha ao apagar o item');
    }
  }

  Future<void> fetchHouseRules({int? page}) async {
    final response = await TodoService.fetchHouseRules();

    if (response != null) {
      setState(() {
        _houseRules = response;
      });
    } else {
      // Show Error!
      showErrorMessage(context, message: 'Falha ao criar nova regra');
    }

    setState(() {
      isLoading = false;
    });
  }
}
