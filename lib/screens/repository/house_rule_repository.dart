import 'dart:convert';

import 'package:crud_app/models/house_rules_model.dart';
import 'package:http/http.dart' as http;

class HouseRuleRepository {
  static const String _baseUrl =
      'https://sys-dev.searchandstay.com/api/admin/house_rules';
  static const String _bearerToken =
      'Bearer 40fe071962846075452a4f6123ae71697463cad20f51e237e2035b41af0513d8';

  Future<List<Entities>> getAllHouseRules() async {
    final uri = Uri.parse(_baseUrl);
    final response = await http.get(
      uri,
      headers: _getHeaders(),
    );
    if (response.statusCode == 201) {
      final json = jsonDecode(response.body);
      final List<dynamic> result = json['data']['entities'];
      return result.map((json) => Entities.fromJson(json)).toList();
    } else {
      throw Exception('Falha ao carregar a lista');
    }
  }

  Future<Entities> createHouseEntity(Entities entity) async {
    final uri = Uri.parse(_baseUrl);
    final response = await http.post(
      uri,
      headers: _getHeaders(),
      body: jsonEncode(entity.toJson()),
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final newer = data['data']['entities'];
      return Entities.fromJson(newer);
    } else {
      throw ('Falha ao criar nova regra');
    }
  }

  Future<Entities> updateHouseRule(int id, Entities entity) async {
    final response = await http.put(
      Uri.parse('$_baseUrl/$id'),
      headers: _getHeaders(),
      body: jsonEncode(entity.toJson()),
    );
    if (response.statusCode == 200) {
      final dynamic responseData =
          json.decode(response.body)["data"]["entities"];
      return Entities.fromJson(responseData);
    } else {
      throw Exception('Failed to update house rule');
    }
  }

  Future<void> deleteHouseRule(int id) async {
    final uri = Uri.parse('$_baseUrl/$id');
    final response = await http.delete(
      uri,
      headers: _getHeaders(),
    );
    if (response.statusCode != 204) {
      throw Exception('Failed to delete house rule');
    }
  }

  Map<String, String> _getHeaders() {
    return {
      'Authorization': _bearerToken,
      'Content-Type': 'application/json',
    };
  }
}
