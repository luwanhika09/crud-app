import 'package:crud_app/models/house_rules_model.dart';
import 'package:crud_app/screens/controller/house_rule_controller.dart';
import 'package:crud_app/services/todo_service.dart';
import 'package:crud_app/utils/snackbar_helper.dart';
import 'package:flutter/material.dart';

class AddTodoPage extends StatefulWidget {
  final Map? todo;
  final Entities? newEnt;
  const AddTodoPage({
    super.key,
    this.todo,
    this.newEnt,
  });

  @override
  State<AddTodoPage> createState() => _AddTodoPageState();
}

class _AddTodoPageState extends State<AddTodoPage> {
  // final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final activeController = TextEditingController();
  final orderController = TextEditingController();
  final controller = HouseRuleController();
  bool isEdit = false;
  late List<dynamic> houseRulesItem = [];
  List<Entities> entitiesList = [];

  @override
  void initState() {
    super.initState();
    loadEntitiesList();
    final todo = widget.todo;
    if (todo != null) {
      isEdit = true;
      // final name = todo['name'];
      // final active = todo['active'];
      // nameController.dispose();
      // activeController.dispose();
    }
  }

  void loadEntitiesList() async {
    final entities = await controller.getEntities();
    setState(() {
      entitiesList = entities;
    });
  }

  Future<void> saveEntity() async {
    final entity = Entities(
      name: nameController.text,
      active: int.parse(activeController.text),
      order: int.parse(orderController.text),
    );

    try {
      await controller.createHouseEntity(entity);
      // Limpa os campos após criar a entidade
      nameController.clear();
      activeController.clear();
    } catch (e) {
      // Handle the error
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(isEdit ? 'Editar a lista' : 'adicionar '),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          TextFormField(
            controller: nameController,
            decoration: const InputDecoration(hintText: 'Nome'),
          ),
          TextFormField(
            controller: activeController,
            decoration: const InputDecoration(hintText: 'Ativo'),
            keyboardType: TextInputType.number,
          ),
          TextFormField(
            controller: orderController,
            decoration: const InputDecoration(hintText: 'Pedido'),
            keyboardType: TextInputType.number,
          ),
          const SizedBox(height: 20),
          ElevatedButton(
            onPressed: isEdit ? updateData : saveEntity,
            style: const ButtonStyle(
                backgroundColor:
                    MaterialStatePropertyAll<Color>(Colors.tealAccent)),
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                isEdit ? 'Atualizar' : 'Criar',
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> updateData() async {
    //  Get the data from form
    final todo = widget.todo;
    if (todo == null) {
      return;
    }
    final id = todo['id'];

    // Submit update data to the server
    final isSuccess = await TodoService.updateTodo(id, body);

    // show success or fail message base on status
    if (isSuccess) {
      showSuccessMessage(context, message: 'Atualizado com Sucesso!');
    } else {
      showErrorMessage(context,
          message: 'Falha ao atualizar os dados tente novamente!');
    }
  }

  Future<void> submitData() async {
    // Submit data to the server
    final isSuccess = await TodoService.addTodo(body);

    // show success or fail message base on status
    if (isSuccess == 200) {
      nameController.text = '';
      activeController.text = '';
      showSuccessMessage(context, message: 'Criado com Sucesso!');
    } else {
      showErrorMessage(context,
          message: 'A Criação falhou, entra em contato com técnico');
    }
  }

  Map get body {
    final name = nameController.text;
    final active = activeController.text;
    // final order = orderController.text;
    return {
      'name': name,
      'active': active,
    };
  }
}
